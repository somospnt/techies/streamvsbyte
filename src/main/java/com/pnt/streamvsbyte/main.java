package com.pnt.streamvsbyte;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class main {

    private static Path pathEntrada = Paths.get("C:\\Users\\usuario\\Documents\\prueba\\Oracle.7z");
    private static Path pathSalida = Paths.get("C:\\Users\\usuario\\Documents\\prueba\\Oracle.7z.copy");

    public static void main(String[] args) throws Exception {
        System.out.println("Comenzar copiado byte");
        System.in.read();
        copyFromBytes();
        System.out.println("Terminado copiado byte");
        System.in.read();
        System.gc();
        System.out.println("-----------------------------------------");
        System.out.println("Comenzar copiado stream");
        System.in.read();
        System.gc();
        copyFromStream();
        System.out.println("Terminado copiado stream");        
        System.gc();
        System.in.read();

    }

    private static void copyFromStream() throws Exception {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(pathEntrada.toFile());
            os = new FileOutputStream(pathSalida.toFile());
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    private static void copyFromBytes() throws IOException {
        byte[] bytes = Files.readAllBytes(pathEntrada);
        Files.write(pathSalida, bytes);
    }

}
